const Bundler = require("parcel-bundler");
const Path = require("path");
const entryFiles = Path.join(__dirname, "./index.js");
const infoAnimation = require("./src/cniel.animation");

process.env.NODE_ENV = 'production';

// Options de l'empaqueteur
const options = {
  outDir: "./dist",
  outFile: `${infoAnimation.key}.cniel-animation.js`,
  watch: false,
  cacheDir: false,
  contentHash: true,
  minify: true,
  sourceMaps: false,
  target: "browser",
};

(async function() {
  const bundler = new Bundler(entryFiles, options);
  await bundler.bundle();
})();
