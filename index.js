/**
 * Base sandbox helper, do not edit.
 */
import animation from "./src/animation";
import { key, name, type, description } from "./src/cniel.animation";

const animator = new CNIELAnimator({
  ...{
    key,
    type,
    name,
    description,
    environment: process.env.NODE_ENV
  },
  ...animation
});

global.CNIELAnimators = global.CNIELAnimators || {};
global.CNIELAnimators[key] = animator.getPublicMethods();

/**
 * Dev helpers.
 */
if (animator.isDev) {
  // Current status.
  const status = !!localStorage.getItem('cnielAnimatorBypassIntro', false);

  // Manage intro message.
  const intro = document.querySelector('.intro-sandbox')

  if (status) {
    intro.parentNode.removeChild(intro)
  } else {
    // Add button event.
    intro.querySelector('.sandbox-close').addEventListener('click', () => {
      localStorage.setItem('cnielAnimatorBypassIntro', true);
      intro.parentNode.removeChild(intro)
    })
  }

}
