const path = require('path')
const CWD = process.cwd()
const infos = require(path.join(CWD, 'src', 'cniel.animation.json'))

const wrapper = async ({name, bundler}) => {
  if (name.split('.').indexOf('cniel-animation') > -1 && bundler.options.production) {
    return {
      header:
`/**
 * @CNIELAnimator {
 *    key: ${infos.key}
 *    name: ${infos.name}
 *    type: ${infos.type}
 *    description: ${infos.description}
 *  }
 */
`,
    }
  }
}

module.exports = wrapper
