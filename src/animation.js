import bodymovinJson from "../assets/bodymovin";

/**
 * @CNIELAnimation
 * A demo implementation of a CNIEL Animation.
 *
 * The "this" namespace can be used as a store for accessing properties and
 * objects across events.
 */
export default {
  /**
   * React when the animation has been attached to the document.
   *
   * @param elt
   *    The DOM HTML element the animation was attached to.
   *    You can access some of the DOM element property like node text or
   *    attributes if needed.
   * @param jquery
   *    The jQuery library.
   * @param bodymovin
   *    The bodymovin NPM library.
   * @param velocity
   *    The Velocity all puroposes animation library.
   */
  onReady(elt, { jquery, bodymovin, velocity }) {
    // Get the title
    const title = jquery(elt)
      .find("h1")
      .text();

    // Remove h1
    jquery(elt)
      .find("h1")
      .remove();

    const animation = bodymovin.loadAnimation({
      container: elt, // the dom element that will contain the animation
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: bodymovinJson, // the path to the animation json
      rendererSettings: {
        scaleMode: 'scale'
      }

    });

    animation.setSpeed(0);

    // Store the animation for later usage.
    this.animation = animation;

    // Exemple of using a custom image.
    // Do not forget to import then image at the top of this file

    // const img = jquery(`<img src="${image}" />`);

    // Do some CSS manipulation
    // img.css({
    //   position: "fixed",
    //   top: 0,
    //   left: 0,
    //   "z-index": 5
    // });
    //
    // jquery(elt).append(img);
  },
  /**
   * React when the cursor is moving inside the animation zone.
   *
   * @param event
   *    The Original MouseEvent
   * @param elt
   *    The DOM HTML element the animation was attached to.
   *    You can access some of the DOM element property like node text or
   *    attributes if needed.
   * @param jquery
   *    The jQuery library.
   * @param bodymovin
   *    The bodymovin NPM library.
   * @param velocity
   *    The Velocity all puroposes animation library.
   */
  onMousemove(event, elt, { jquery, bodymovin, velocity }) {
    console.log('Mouse is hover the animation zone')
  },
  /**
   * React when the document is resized.
   *
   * @param infos
   *    Information about the document size
   * @param elt
   *    The DOM HTML element the animation was attached to.
   *    You can access some of the DOM element property like node text or
   *    attributes if needed.
   * @param jquery
   *    The jQuery library.
   * @param bodymovin
   *    The bodymovin NPM library.
   * @param velocity
   *    The Velocity all puroposes animation library.
   */
  onResize(infos, elt, { jquery, bodymovin, velocity }) {
    console.log("Document resize");
  },
  /**
   * React when the cursor is moving in the whole document viewport.
   *
   * @param infos
   *    Information about the cursor position & velocity
   * @param elt
   *    The DOM HTML element the animation was attached to.
   *    You can access some of the DOM element property like node text or
   *    attributes if needed.
   * @param jquery
   *    The jQuery library.
   * @param bodymovin
   *    The bodymovin NPM library.
   * @param velocity
   *    The Velocity all puroposes animation library.
   */
  onDocumentMouseMove(infos, elt, { jquery, bodymovin, velocity }) {
    const velocityAbs = Math.sqrt(
      infos.velocity.x ** 2 + infos.velocity.y ** 2
    );

    this.animation.setSpeed(velocityAbs / 100);
  },
  /**
   * React when the document is being scrolled into.
   * NB : If you need to detect animation visibility, use the
   * IntersectionObserver API
   *
   * @param infos
   *    Information about the scroll position
   * @param elt
   *    The DOM HTML element the animation was attached to.
   *    You can access some of the DOM element property like node text or
   *    attributes if needed.
   * @param jquery
   *    The jQuery library.
   * @param bodymovin
   *    The bodymovin NPM library.
   * @param velocity
   *    The Velocity all puroposes animation library.
   */
  onScroll(infos, elt, { jquery, bodymovin, velocity }) {
    console.log("Document scroll");
  }
};
