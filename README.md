# Les produits laitiers - Sandbox d'animation

## Philosophie
Ce prototype simplifie et sécurise l'implémentation d'animations avancées dans le nouveau site https://www.produits-laitiers.com/ .

Cette sandbox requiert des connaissances moyennes à avancée en Javascript (ES6+) selon la difficultées des animations voulant être implémentée.

Elle est livrée avec les outils (npm) suivants :
* jQuery (https://www.npmjs.com/package/jquery)
* bodymovin (https://www.npmjs.com/package/bodymovin)
* velocity (https://www.npmjs.com/package/velocityjs)

Les animations sont conçues pour être implémentées dans des zones définies au préalable :
* Haut de la page d'accueil (**homepage**),
* Heromodule d'une verticale (**hero**),
* Fond du push d'articles au sein d'une verticale (**pushPost**),
* Fond de la petite photo d'un influenceur dans le listing d'article (**personaSmall**)
* Fond de la grande photo d'un influenceur sur sa page dédiée (**personaBig**)
* Zone d'illustration du Top 10 (**top10**)

## Implémentation

1. Dans le dossier /src modifier le fichier cniel.animation.json de façon à entrer les informations utiles à cette animation. La liste des types d'animations disponible est listée au dessus.
1. Le fichier d'entrée est /src/animation.js.
1. Vos animations s'appuient sur les callbacks déclanchés a plusieurs moments de la navigation/interaction cliente. Vous référer à la documentation du code pour en savoir plus.
1. Vous pouvez _importer_ vos différents scripts et assets dans le fichier animation.js
1. Lorsque terminée, l'animation doit être exportée via "yarn build".

## Dépot sur le site
Le fichier disponible dans /dist doit être ensuite confié aux équipes de webmastering du CNIEL de façon à charger sur le site et à implémenter l'animation où souhaitée.
